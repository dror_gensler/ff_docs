.. FunctionFactory documentation master file, created by
   sphinx-quickstart on Mon Feb 19 15:32:06 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Basic Usage
===========

Creating Components
-------------------

A serverless function must follow this simple rule:
It accepts a **dictionary** (object/array) as input, and **must** return a dictionary as output.

Creating serverless functions / components is easy using our web-based editor on the left half of the page.
Either edit the selected component, or create a new one:

click the component name at the top-left and select "**New**" from the dropdown list.

For example, here is a simple "Hello world!" component: 
::
  def main(params):

     params['msg'] = 'Hello world!'

  return params

We start by creating a **main()** function which will be the entrypoint to our code (no need to call it, the platform will always invoke *main*).

As you can see, we are recieveing the **params** dictionary as input, assign our message as a new key in it, and return the same dictionary.

We don't have to return the same dictionary. We can always create and return a new one - 
:: 
  def main(params):

     msg = 'Hello world!'

  return {'out': msg}


Save
----
Click the **Save** button at the top of the page.
This saves the editor code to the selected component (name appears top left), overwriting it and incrementing its version number.

The component is saved to your **private** library.
Private components are listed in the drop-down list at the top-left.

.. note::
  Unsaved changes will be lost if you select a different component from the drop-down or navigate away.


Run
---
Once changes are saved click **Run** to invoke your component.
This will execute the component with an "empty" input dictionary ( except for the default environment variables, keys and such).

Results will be presented below the editor as soon as execution is completed.
Do note the error messages presented in the *Logs* field of the output to debug your function.

To invoke components using an external request - Please see next section - **Publish**
