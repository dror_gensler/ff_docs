.. FunctionFactory documentation master file, created by
   sphinx-quickstart on Mon Feb 19 15:32:06 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
Welcome to FunctionFactory documentation!
******************************************


.. toctree::
   :maxdepth: 4
   :caption: Documentation

   basic
   publish 
   library
   reference

.. toctree::
   :maxdepth: 2
   :caption: Guides

   tutorial_weather
   tutorial_contact
  
.. toctree::
   :maxdepth: 0
   :caption: Terms

   customer_agreement
   acceptable_use
  
