Component Library
=================

The component library is available on the right half of the screen.
Use the search bar at the top-right to submit a query.

Results are presented as discussion topics - questions and answers, topic and subtopics, etc.
Click on a relevant result to expand it and see replies. 

Each reply can have a component attached to it.
Click on **Inspect** to review the component code, and if satisfied - 
Click **Add** to inject a line to your editor (after cursor position) calling that component.

Make sure you import the ``ffCall`` function to execute the call.
At the beginning of your code, add:

Python - ``from ff import ffCall``  

NodeJS - ``ffCall = require('ff').ffCall;``

Calling Components
------------------
To call a component from within a component, first import the "ff" (FunctionFactory) library:  

Python
::
  from ff import ffCall
NodeJS
:: 
  ffCall = require('ff').ffCall;

Then, use the ``ffCall`` function, giving it a component name and a dictionary/array of input parameters:  
::
  ffCall('component-name', array)
  
Component name can be either 'name' - to call a component in your private library (seen above),  
Or 'user/name-version' - to call a component from the public library.

Public library call:
::
  ffCall('david/getWeather-0.6', array)

Remember to assign the output to a parameter to be returned.  


