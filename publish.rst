.. FunctionFactory documentation master file, created by
   sphinx-quickstart on Mon Feb 19 15:32:06 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Publish
-------
The *Publish* button will open the overlay containing the three options:

Copy to public library
~~~~~~~~~~~~~~~~~~~~~~
Before you can attach a component to a discussion reply, you must first copy it to your space in the public library, where other users can access it.  

A **copy** of your private component will be saved in the public library under your space with a **version** number added (i.e <username>/<component>-0.0.12).

.. note::
    Components copied to the public library are visible to all and **cannot be edited**.
    Make sure there are no secrets / credentials hardcoded!

You can still edit and save your private component code, but every time you copy it to your public library a new version is created, with the old ones kept.

Private REST endpoint
~~~~~~~~~~~~~~~~~~~~~
Click the "Show" button to reveal the private REST endpoint.  
This endpoint triggers the component when it recieves a **POST** request.  

The output is a JSON array containing the "result" key with the returned output.  

To pass parameters to this endpoint you must specify input data as a JSON array and JSON as the content-type like so::  

  curl -X POST https://<secret-key>@functionfactory.io/api/v1/namespaces/<user>/actions/private/<component>?blocking=true \ 
  -d '{"myKey": "myValue"}' \ 
  -H 'Content-Type: application/json'  

Every private component has a private endpoint assigned by default.
You can save your secret key which is shown here and use it with other components as well (but **keep it secret**).

Public Endpoint
~~~~~~~~~~~~~~~
In the "Publish" modal click the "Expose" button to create a web-exposed endpoint.  
This endpoint does not require a key to trigger.  
It can be triggered using a browser, or by a GET request:  

``curl https://functionfactory.io/api/v1/web/<user>/private/<component>``  

To return results we must alter our code to signal the API gateway that this is a public component.
We do this by altering the ``return`` statement of our ``main()`` function to return a ``body`` key:
::
  def main(params):
    
    params['msg'] = 'Hello world!'

  return {'body': params}

The returned **body** key can be an array, object or even a string.

You could return HTML and create a page that is driven entirely by serverless components:
::
  def main(params):
    name = params.get('name', 'world!')
    greeting = 'Hello ' + name

  return {'body': '<html>' + params['greeting'] + '</html>'}

Pass parameters to a web-exposed component as you would to a web site, using URL parameters-  

``curl https://functionfactory.io/api/v1/web/<user>/private/helloWorld?name=David``  

