.. Reference page

Reference
=========

Below is reference imformation for creating components and interacting with the platform externally (via REST or CLI utils).

Installed Libraries
-------------------

Component code is executed from template containers which come pre-installed with some libraries.
Below is a reference list of all pre-installed libraries.
If you find missing dependencies which you believe to be necessary - do let us know and we'll add it!


Python:2
~~~~~~~~

- appdirs v1.4.3
- asn1crypto v0.21.1
- attrs v16.3.0
- beautifulsoup4 v4.5.1
- cffi v1.9.1
- click v6.7
- cryptography v1.8.1
- cssselect v1.0.1
- enum34 v1.1.6
- Flask v0.11.1
- gevent v1.1.2
- greenlet v0.4.12
- httplib2 v0.9.2
- idna v2.5
- ipaddress v1.0.18
- itsdangerous v0.24
- Jinja2 v2.9.5
- kafka-python v1.3.1
- lxml v3.6.4
- MarkupSafe v1.0
- packaging v16.8
- parsel v1.1.0
- pyasn1 v0.2.3
- pyasn1-modules v0.0.8
- pycparser v2.17
- PyDispatcher v2.0.5
- pyOpenSSL v16.2.0
- pyparsing v2.2.0
- python-dateutil v2.5.3
- queuelib v1.4.2
- requests v2.11.1
- Scrapy v1.1.2
- service-identity v16.0.0
- simplejson v3.8.2
- six v1.10.0
- Twisted v16.4.0
- virtualenv v15.1.0
- w3lib v1.17.0
- Werkzeug v0.12
- zope.interface v4.3.3


Node.js:6
~~~~~~~~~
- `apn v2.1.2 <https://www.npmjs.com/package/apn>`_ - A Node.js module for interfacing with the Apple Push Notification service.
- `async v2.1.4 <https://www.npmjs.com/package/async>`_ - Provides functions for working with asynchronous functions.
- `btoa v1.1.2 <https://www.npmjs.com/package/btoa>`_ - A port of the browser's btoa function.
- `cheerio v0.22.0 <https://www.npmjs.com/package/cheerio>`_ - Fast, flexible & lean implementation of core jQuery designed specifically for the server.
- `cloudant v1.6.2 <https://www.npmjs.com/package/cloudant>`_ - This is the official Cloudant library for Node.js.
- `commander v2.9.0 <https://www.npmjs.com/package/commander>`_ - The complete solution for Node.js command-line interfaces.
- `consul v0.27.0 <https://www.npmjs.com/package/consul>`_ - A client for Consul, involving service discovery and configuration.
- `cookie-parser v1.4.3 <https://www.npmjs.com/package/cookie-parser>`_ - Parse Cookie header and populate req.cookies with an object keyed by the cookie names.
- `cradle v0.7.1 <https://www.npmjs.com/package/cradle>`_ - A high-level, caching, CouchDB client for Node.js.
- `errorhandler v1.5.0 <https://www.npmjs.com/package/errorhandler>`_ - Development-only error handler middleware.
- `glob v7.1.1 <https://www.npmjs.com/package/glob>`_ - Match files by using patterns that the shell uses, like stars and stuff.
- `gm v1.23.0 <https://www.npmjs.com/package/gm>`_ - GraphicsMagick and ImageMagick for Node.
- `lodash v4.17.2 <https://www.npmjs.com/package/lodash>`_ - The Lodash library exported as Node.js modules.
- `log4js v0.6.38 <https://www.npmjs.com/package/log4js>`_ - A conversion of the log4js framework designed to work with Node.
- `iconv-lite v0.4.15 <https://www.npmjs.com/package/iconv-lite>`_ - Pure JS character encoding conversion
- `marked v0.3.6 <https://www.npmjs.com/package/marked>`_ - A full-featured markdown parser and compiler, which is written in JavaScript. Built for speed.
- `merge v1.2.0 <https://www.npmjs.com/package/merge>`_ - Merge multiple objects into one, optionally creating a new cloned object.
- `moment v2.17.0 <https://www.npmjs.com/package/moment>`_ - A lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates.
- `mongodb v2.2.11 <https://www.npmjs.com/package/mongodb>`_ - The official MongoDB driver for Node.js.
- `mustache v2.3.0 <https://www.npmjs.com/package/mustache>`_ - Mustache.js is an implementation of the mustache template system in JavaScript.
- `nano v6.2.0 <https://www.npmjs.com/package/nano>`_ - Minimalistic couchdb driver for Node.js.
- `node-uuid v1.4.7 <https://www.npmjs.com/package/node-uuid>`_ - Deprecated UUID packaged.
- `nodemailer v2.6.4 <https://www.npmjs.com/package/nodemailer>`_ - Send e-mails from Node.js – easy as cake!
- `oauth2-server v2.4.1 <https://www.npmjs.com/package/oauth2-server>`_ - Complete, compliant, and well tested module for implementing an OAuth2 Server/Provider with express in Node.js.
- `openwhisk v3.12.0 <https://www.npmjs.com/package/openwhisk>`_ - JavaScript client library for the OpenWhisk platform. Provides a wrapper around the OpenWhisk APIs.
- `pkgcloud v1.4.0 <https://www.npmjs.com/package/pkgcloud>`_ - pkgcloud is a standard library for Node.js that abstracts away differences among multiple cloud providers.
- `process v0.11.9 <https://www.npmjs.com/package/process>`_ - Require<'process'>`_; just like any other module.
- `pug v2.0.0-beta6 <https://www.npmjs.com/package/pug>`_ - Implements the Pug templating language.
- `redis v2.6.3 <https://www.npmjs.com/package/redis>`_ - This is a complete and feature-rich Redis client for Node.js.
- `request v2.79.0 <https://www.npmjs.com/package/request>`_ - Request is the simplest way possible to make HTTP calls.
- `request-promise v4.1.1 <https://www.npmjs.com/package/request-promise>`_ - The simplified HTTP request client 'request' with Promise support. Powered by Bluebird.
- `rimraf v2.5.4 <https://www.npmjs.com/package/rimraf>`_ - The UNIX command rm -rf for node.
- `semver v5.3.0 <https://www.npmjs.com/package/semver>`_ - Supports semantic versioning.
- `sendgrid v4.7.1 <https://www.npmjs.com/package/sendgrid>`_ - Provides email support via the SendGrid API.
- `serve-favicon v2.3.2 <https://www.npmjs.com/package/serve-favicon>`_ - Node.js middleware for serving a favicon.
- `socket.io v1.6.0 <https://www.npmjs.com/package/socket.io>`_ - Socket.IO enables real-time bidirectional event-based communication.
- `socket.io-client v1.6.0 <https://www.npmjs.com/package/socket.io-client>`_ - Client-side support for Socket.IO.
- `superagent v3.0.0 <https://www.npmjs.com/package/superagent>`_ - SuperAgent is a small progressive client-side HTTP request library, and Node.js module with the same API, sporting many high-level HTTP client features.
- `swagger-tools v0.10.1 <https://www.npmjs.com/package/swagger-tools>`_ - Tools that are related to working with Swagger, a way to document APIs.
- `tmp v0.0.31 <https://www.npmjs.com/package/tmp>`_ - A simple temporary file and directory creator for node.js.
- `twilio v2.11.1 <https://www.npmjs.com/package/twilio>`_ - A wrapper for the Twilio API, related to voice, video, and messaging.
- `underscore v1.8.3 <https://www.npmjs.com/package/underscore>`_ - Underscore.js is a utility-belt library for JavaScript that supports the usual functional suspects <each, map, reduce, filter...>`_ without extending any core JavaScript objects.
- `uuid v3.0.0 <https://www.npmjs.com/package/uuid>`_ - Simple, fast generation of RFC4122 UUIDS.
- `validator v6.1.0 <https://www.npmjs.com/package/validator>`_ - A library of string validators and sanitizers.
- `watson-developer-cloud v2.29.0 <https://www.npmjs.com/package/watson-developer-cloud>`_ - Node.js client library to use the Watson Developer Cloud services, a collection of APIs that use cognitive computing to solve complex problems.
- `when v3.7.7 <https://www.npmjs.com/package/when>`_ - When.js is a rock solid, battle-tested Promises/A+ and when<>`_ implementation, including a complete ES6 Promise shim.
- `winston v2.3.0 <https://www.npmjs.com/package/winston>`_ - A multi-transport async logging library for node.js. "CHILL WINSTON! ... I put it in the logs."
- `ws v1.1.1 <https://www.npmjs.com/package/ws>`_ - ws is a simple to use, blazing fast, and thoroughly tested WebSocket client and server implementation.
- `xml2js v0.4.17 <https://www.npmjs.com/package/xml2js>`_ - Simple XML to JavaScript object converter. It supports bi-directional conversion.
- `xmlhttprequest v1.8.0 <https://www.npmjs.com/package/xmlhttprequest>`_ - node-XMLHttpRequest is a wrapper for the built-in http client to emulate the browser XMLHttpRequest object.
- `yauzl v2.7.0 <https://www.npmjs.com/package/yauzl>`_ - Yet another unzip library for node. For zipping.

Node.js:8
~~~~~~~~~
- `apn  <https://www.npmjs.com/package/apn>`_ - A Node.js module for interfacing with the Apple Push Notification service.
- `async  <https://www.npmjs.com/package/async>`_ - Provides functions for working with asynchronous functions.
- `btoa  <https://www.npmjs.com/package/btoa>`_ - A port of the browser's btoa function.
- `cheerio  <https://www.npmjs.com/package/cheerio>`_ - Fast, flexible & lean implementation of core jQuery designed specifically for the server.
- `cloudant  <https://www.npmjs.com/package/cloudant>`_ - This is the official Cloudant library for Node.js.
- `commander  <https://www.npmjs.com/package/commander>`_ - The complete solution for Node.js command-line interfaces.
- `consul  <https://www.npmjs.com/package/consul>`_ - A client for Consul, involving service discovery and configuration.
- `cookie-parser  <https://www.npmjs.com/package/cookie-parser>`_ - Parse Cookie header and populate req.cookies with an object keyed by the cookie names.
- `cradle  <https://www.npmjs.com/package/cradle>`_ - A high-level, caching, CouchDB client for Node.js.
- `errorhandler  <https://www.npmjs.com/package/errorhandler>`_ - Development-only error handler middleware.
- `glob  <https://www.npmjs.com/package/glob>`_ - Match files by using patterns that the shell uses, like stars and stuff.
- `gm  <https://www.npmjs.com/package/gm>`_ - GraphicsMagick and ImageMagick for Node.
- `lodash  <https://www.npmjs.com/package/lodash>`_ - The Lodash library exported as Node.js modules.
- `log4js  <https://www.npmjs.com/package/log4js>`_ - A conversion of the log4js framework designed to work with Node.
- `iconv-lite  <https://www.npmjs.com/package/iconv-lite>`_ - Pure JS character encoding conversion
- `marked  <https://www.npmjs.com/package/marked>`_ - A full-featured markdown parser and compiler, which is written in JavaScript. Built for speed.
- `merge  <https://www.npmjs.com/package/merge>`_ - Merge multiple objects into one, optionally creating a new cloned object.
- `moment  <https://www.npmjs.com/package/moment>`_ - A lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates.
- `mongodb  <https://www.npmjs.com/package/mongodb>`_ - The official MongoDB driver for Node.js.
- `mustache  <https://www.npmjs.com/package/mustache>`_ - Mustache.js is an implementation of the mustache template system in JavaScript.
- `nano  <https://www.npmjs.com/package/nano>`_ - Minimalistic couchdb driver for Node.js.
- `node-uuid  <https://www.npmjs.com/package/node-uuid>`_ - Deprecated UUID packaged.
- `nodemailer  <https://www.npmjs.com/package/nodemailer>`_ - Send e-mails from Node.js – easy as cake!
- `oauth2-server  <https://www.npmjs.com/package/oauth2-server>`_ - Complete, compliant, and well tested module for implementing an OAuth2 Server/Provider with express in Node.js.
- `openwhisk  <https://www.npmjs.com/package/openwhisk>`_ - JavaScript client library for the OpenWhisk platform. Provides a wrapper around the OpenWhisk APIs.
- `pkgcloud  <https://www.npmjs.com/package/pkgcloud>`_ - pkgcloud is a standard library for Node.js that abstracts away differences among multiple cloud providers.
- `process  <https://www.npmjs.com/package/process>`_ - Require<'process'>`_; just like any other module.
- `pug v2.0.0-beta6 <https://www.npmjs.com/package/pug>`_ - Implements the Pug templating language.
- `redis  <https://www.npmjs.com/package/redis>`_ - This is a complete and feature-rich Redis client for Node.js.
- `request  <https://www.npmjs.com/package/request>`_ - Request is the simplest way possible to make HTTP calls.
- `request-promise  <https://www.npmjs.com/package/request-promise>`_ - The simplified HTTP request client 'request' with Promise support. Powered by Bluebird.
- `rimraf  <https://www.npmjs.com/package/rimraf>`_ - The UNIX command rm -rf for node.
- `semver  <https://www.npmjs.com/package/semver>`_ - Supports semantic versioning.
- `sendgrid  <https://www.npmjs.com/package/sendgrid>`_ - Provides email support via the SendGrid API.
- `serve-favicon  <https://www.npmjs.com/package/serve-favicon>`_ - Node.js middleware for serving a favicon.
- `socket.io  <https://www.npmjs.com/package/socket.io>`_ - Socket.IO enables real-time bidirectional event-based communication.
- `socket.io-client  <https://www.npmjs.com/package/socket.io-client>`_ - Client-side support for Socket.IO.
- `superagent  <https://www.npmjs.com/package/superagent>`_ - SuperAgent is a small progressive client-side HTTP request library, and Node.js module with the same API, sporting many high-level HTTP client features.
- `swagger-tools  <https://www.npmjs.com/package/swagger-tools>`_ - Tools that are related to working with Swagger, a way to document APIs.
- `tmp  <https://www.npmjs.com/package/tmp>`_ - A simple temporary file and directory creator for node.js.
- `twilio  <https://www.npmjs.com/package/twilio>`_ - A wrapper for the Twilio API, related to voice, video, and messaging.
- `underscore  <https://www.npmjs.com/package/underscore>`_ - Underscore.js is a utility-belt library for JavaScript that supports the usual functional suspects <each, map, reduce, filter...>`_ without extending any core JavaScript objects.
- `uuid  <https://www.npmjs.com/package/uuid>`_ - Simple, fast generation of RFC4122 UUIDS.
- `validator  <https://www.npmjs.com/package/validator>`_ - A library of string validators and sanitizers.
- `watson-developer-cloud  <https://www.npmjs.com/package/watson-developer-cloud>`_ - Node.js client library to use the Watson Developer Cloud services, a collection of APIs that use cognitive computing to solve complex problems.
- `when  <https://www.npmjs.com/package/when>`_ - When.js is a rock solid, battle-tested Promises/A+ and when<>`_ implementation, including a complete ES6 Promise shim.
- `winston  <https://www.npmjs.com/package/winston>`_ - A multi-transport async logging library for node.js. "CHILL WINSTON! ... I put it in the logs."
- `ws  <https://www.npmjs.com/package/ws>`_ - ws is a simple to use, blazing fast, and thoroughly tested WebSocket client and server implementation.
- `xml2js  <https://www.npmjs.com/package/xml2js>`_ - Simple XML to JavaScript object converter. It supports bi-directional conversion.
- `xmlhttprequest  <https://www.npmjs.com/package/xmlhttprequest>`_ - node-XMLHttpRequest is a wrapper for the built-in http client to emulate the browser XMLHttpRequest object.
- `yauzl  <https://www.npmjs.com/package/yauzl>`_ - Yet another unzip library for node. For zipping.

System Limits
-------------

The execution environemt has a few limits, including max execution time, size and memory usage.
Some values will be increased post-beta.

+------------+-------------------------------------+------------+-------+
| Limit      | Description                         | Unit       | Amount|
+============+=====================================+============+=======+
| timeout    | component max execution time        |milliseconds|60000  |
+------------+-------------------------------------+------------+-------+
| logs       | component can write N MB to stdout  |MB          |10     |
+------------+-------------------------------------+------------+-------+
| concurrent | max simultaneous activation         |number      |100    |
+------------+-------------------------------------+------------+-------+
| minuteRate | max activations per minute          |number      |60     |
+------------+-------------------------------------+------------+-------+
| parameters | max size of parameters object passed|MB          |1      |
+------------+-------------------------------------+------------+-------+
| result     | max size of result object returned  |MB          |1      |
+------------+-------------------------------------+------------+-------+
