Tutorial 2 - Web Form
=====================

In this tutorial we will create a "**web**" type component, which can be viewed using a browser and requires no authentication.  
We will utilize library components to create new behaviour using our own wrapper component, returning HTML-ish output.

Creating a component
--------------------

First, we must create a new component to work on.  
Click on the current component name at the top-left of the screen to open the dropdown.  
Apart from listing your private components - this is where the "**New**" button is found which.. creates new components..  

Click **New**, and in the dialog that opens name your new component and select the programming language to use,
And click "**Create**".

(The code in this tutorial is in **Python:2**)

Great. Now we have a blank editor to populate.
Lets take care of the essentials -  
A component needs a **main** function to **accept** a parameters dictonary and **return** a dictionary.  
A basic main function will look like so:
::
  def main(params):
    
      header1 = '<h1>Contact Form</h1>'

      return {'body': header1}

We've set the ``return`` value to a Python dictionary (``{}``), because all components **must** return a dictionary.  
``retrun header1`` would result in an error.

Also, we placed our content (header1) in the special key ``body`` -  
This indicates to the platform that this is a web-type action, and it returns a cleaner output.  

(If we wanted to create an API endpoint with JSON output we could set the key to whatever we wanted.)  

Click **Save** and then **Run** at the top and you should see the results in the output section below the editor.

Enable Web Access
-----------------

Since we are creating a web page we need to enable access to random, unauthenticated people from the outside world.
To do that - click the **Publish** button at the top of the screen, and scroll down to the third option - "Public Web Endpoint".

Click **Expose** to enable access and reveal the public endpoint URL for this component.  

Now copy-paste the URL to a new browser tab, and you should see the output displayed as a page.


Email Contact Form
------------------

Now lets get to the actual inputs of our contact form, which we not only need to collect, but also send or store
somewhere upon submission.

Lets check the library for a component that does that!

Type "**contact form**" into the search bar at the top-right.
There should be a similar question that has already been asked.
Click the relevant one to reveal the answers.

One answer should have the "**drorg/formspree-0.0.13**" component attached.  

If you click "**INSPECT**", you can see the actual code. This helps determine the input parameters required for this component to function,
As well as the structure of the returned dictionary.

Click "**Add**" to append a line calling this component to our editor.

Note - The line will be added below the current cursor position.

Before you save
~~~~~~~~~~~~~~~

If you were to Save and Run now you'd get an error message - ``ffCall is not defined``.

We must first import the ffCall function by adding a line to the first line in our editor:
:: 
  from ff import ffCall

Also, we need to add the formspree form to our returned ``body`` key.
The ``formspree`` component returns the key ``form`` containing the HTML inputs.

Add a line concatinating ``header1`` with the ffCall output to create a variable containing the complete page:
:: 
  full = header1 + "<br/>" + params['form']

And alter the returned body to be ``full``.

So our component code should look something like this now:
:: 
  from ff import ffCall

  def main(params):

      header1 = '<h1>Contact Form</h1>'

      params = ffCall("drorg/formspree-0.0.13", params)
    
      full = header1 + "<br/>" + params['form']

      return {'body': full}


Now when we click "**Run**" we should get the HTML form in the output section,
And we can see the input fields and submit button by navigating to the public URL.

But! If we fill the form and click "submit" - we get an error.  
We have yet to set the e-mail address to which the form is sent to..

Setting Input Parameters
------------------------

If we read the answer description or **INSPECT** the code we see the ``formspree`` component requires that
we pass it an ``email`` parameter, to which the form will be sent.

The parameter must be passed to the component in a single input dictionary. like in all other components.
So we create a new dictionary to hold and pass it to formspree -
::
  data = {'email': '<your email>'}

Lets add that line before the call to formspree, and make sure we give it the **data** dictionary instead of the default **params** as input!
::
  from ff import ffCall

  def main(params):

      header1 = '<h1>Contact Form</h1>'

      data = {'email': 'yourmail@email.com'}
      params = ffCall("drorg/formspree-0.0.13", data)
    
      full = header1 + "<br/>" + params['form']

      return {'body': full}

Again make sure the second argument in ffCall is our ``data`` dict, not ``params``.

A quick **Save**, reload the public endpoint browser tab, and you should see the same form,
But this time, when you submit it - it will end up in your inbox!

Lovely!

You will probably notice that we assign the output of the ``ffCall`` to ``params``, thus destroying the original inputs to our function.
Of course we could assign each calls' output to its own variable, and return whichever variable we wanted.
