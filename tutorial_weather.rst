Tutorial 1 - Weather API
========================

In this tutorial we will utilize library components to create new behaviour using our own wrapper component.

Creating a component
--------------------

First, we must create a new component to work on.
Click on the current component name at the top-left of the screen to open the dropdown.
Apart from listing your private components - this is where the "**New**" button is found which.. creates new components.. 

Click **New**, and in the dialog that opens name your new component and select the programming language to use.
Lets stick to **Python:2** for this tutorial.
And click "Create".

Great. Now we have a blank editor to populate.
Lets take care of the essentials - 
A component needs a **main** function to **accept** a parameters dictonary and **return** a dictionary.
A basic main function will look like so:
::
  def main(params):
    
    params['msg'] = 'hello!'

    return params

We've set the ``params['msg']`` key to put *something* in params, otherwise we'd get an error when a blank dictionary will be returned.

Click **Save** and then **Run** at the top and soon after you should see the results in the output section below the editor.

Get weather data
----------------

Lets say we want some weather information to work with.
In this situation, I'd usually go Googling for a free weather API service.

Lets check the library instead!

Type "**weather api**" into the search bar at the top-right.
There should be a similar question that has already been asked.
Click the relevant one to reveal the answers.

I chose to use "**drors/metaWeather-0.0.2**" .
Clich "**Add**" to append a line calling this component to our editor.

Note - The line will be added below the current cursor position.

Before you save
~~~~~~~~~~~~~~~

If you were to Save and Run now you'd get an error message - ``ffCall is not defined``.

We must first import the ffCall function by adding a line to the first line in our editor:
:: 
  from ff import ffCall

So our component code should look something like this now:
:: 
  from ff import ffCall

  def main(params):
    
    params = ffCall("drors/metaWeather-0.0.2", params)

    return params
 
(I removed the msg line. we don't need it.)

Now when we click "**Run**" we should get some (lots) of weather data in the output section.
(Takes a second or two because metaWeather service, while free - is slow).

Sample output:
::
  Response : 
              Status : success
                          Result : {"weather":{"parent":{"woeid":24554868,"latt_long":"52.883560,-1.974060","location_type":"Region / State / Province","title":"England"},"title":"London","woeid":44418,"sun_set":"2018-02-20T17:24:31.734309Z","consolidated_weather":[{"wind_speed":7.1347790396309545,"applicable_date":"2018-02-


Wow that's a lot of output! All I wanted was the temprature! 
Lets see if we can reduce it..

Filtering results
-----------------

A closer look at the output of metaWeather reveals it to be a **complex object** or a **nested array**.
The result object is a mix of nested dictionaries (``{}``) and lists (``[]``).
To extract a specific key we'd have to iterate each level, figure out its type, assign it, open the next level etc etc.. 

What we need is an **iterator** to find a specific key in this object.
Again lets turn to the library!

Search for "**find key**" or "**iterate array**".

I went with "**drors/findKeyImproved-0.0.6**".
Click on "**Add**" to append an **ffCall** line below our metaWeather call.

In the description of this findKey component, and in the code itself (if you click "inspect"), we see that this component expects two parametes be given -

"**findKey**" - the key to be found, and
"**object**" - the complex object to search in.

These two parameters must be passed to the component in a single input dictionary. like in all other components.
So we create a new dictionary to hold and pass them to findKey -
::
  data = {'findKey': 'the_temp', 'object': params['weather']}

I chose to extract **the_temp** (temprature), and am passing **params['weather']**, which was the output of the previous component, as the object to iterate over.

Lets add that line before the call to findKey, and make sure we give it the **data** dictionary instead of the default **params** as input!
::
  from ff import ffCall

  def main(params):
    
    params = ffCall("drors/metaWeather-0.0.2", params)

    data = {'findKey': 'the_temp', 'object': params['weather']}
    params = ffCall("drors/findKeyImproved-0.0.6", data)

    return params

Again make sure the second argument in ffCall is our ``data`` dict, not ``params``.

A quick **Save** and **Run** and the output should be cleaner - 
::
  Result : {"found":[6.029999999999999,4.22,3.77,4.220000000000001,2.35,3.04]}

Lovely!

You will probably notice that we assign the output of the second ``ffCall`` to ``params`` again, thus destroying the output of the first call to metaWeather.
Of course we could assign each calls' output to its own variable, and return whichever variable we wanted.

We could also fiddle with these results further, maybe make an average of the four readings,
And since you are already in a Python environment - that is easy to do.

Congrats! 
 
